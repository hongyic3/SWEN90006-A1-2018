package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }


  @Test public void B1TestCase()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    List<String> list = new ArrayList<String>();
    list.add("MOV R0 0");
    list.add("RET R0");
    Machine machine = new Machine();
    int actual = 0; // expected result
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B2TestCase()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 2");
    list.add("ADD R31 R1 R2");
    list.add("RET R31");
    Machine machine = new Machine();
    int actual = 31;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B5TestCase()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -65535");
    list.add("MOV R2 1");
    list.add("DIV R3 R1 R2");
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = -65535;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B6TestCase()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 1");
    list.add("SUB R3 R1 R2");
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 65534;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B9TestCase()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    List<String> list = new ArrayList<String>();
    list.add("JMP 0");
    list.add("MOV R1 0");
    list.add("RET R1");
    Machine machine = new Machine();
    int actual = 0; // It should be a infinite loop.
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B10TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 10");
    list.add("MOV R2 12");
    list.add("JMP 2");
    list.add("RET R1");
    list.add("RET R2");
    Machine machine = new Machine();
    int actual = 12;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B13TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 100");
    list.add("STR R1 -65535 R2");
    list.add("LDR R3 R1 -65535");
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 100;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B14TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -65535");
    list.add("MOV R2 100");
    list.add("STR R1 65535 R2");
    list.add("LDR R3 R1 65535");
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 100;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B15TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 100");
    list.add("STR R1 0 R2");// instruction does nothing
    list.add("LDR R3 R1 0");// instruction does nothing
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 100;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B16TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 0");
    list.add("MOV R2 100");
    list.add("STR R1 65535 R2");// instruction does nothing
    list.add("LDR R3 R1 65535");// instruction does nothing
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 100;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B17TestCase()
{
  List<String> list = new ArrayList<String>();
  list.add("MOV R1 65534");
  list.add("MOV R2 100");
  list.add("MOV R3 333");
  list.add("STR R1 -65535 R2");// instruction does nothing
  list.add("LDR R3 R1 -65535");// instruction does nothing
  list.add("RET R3");
  Machine machine = new Machine();
  int actual = 333;
  assertEquals(machine.execute(list), actual);
}

  @Test public void B18TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -65535");
    list.add("MOV R2 100");
    list.add("MOV R3 333");
    list.add("STR R1 65534 R2");// instruction does nothing
    list.add("LDR R3 R1 65534");// instruction does nothing
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 333;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B19TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 100");
    list.add("MOV R3 333");
    list.add("STR R1 1 R2");// instruction does nothing
    list.add("LDR R3 R1 1");// instruction does nothing
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 333;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void B20TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 100");
    list.add("MOV R3 333");
    list.add("STR R1 65535 R2");// instruction does nothing
    list.add("LDR R3 R1 65535");// instruction does nothing
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 333;
    assertEquals(machine.execute(list), actual);
  }
//-------------------------------
  //To test an exception, specify the expected exception after the @Test

  @Test(expected = NoReturnValueException.class)
  public void B0anNoReturnValueExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add(""); //EMPTY LIST
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void B3anInvalidInstructionExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R-1 -1");
    list.add("RET R-1");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void B4anInvalidInstructionExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R32 32");
    list.add("RET R32");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void B7anInvalidInstructionExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R2 -65536");
    list.add("RET R2");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void B8anInvalidInstructionExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R3 65536");
    list.add("RET R3");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = NoReturnValueException.class)
  public void B11anNoReturnValueExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 10");
    list.add("JMP -2");
    list.add("RET R1");
    Machine machine = new Machine();
    machine.execute(list);
  }


  @Test(expected = NoReturnValueException.class)
  public void B12anNoReturnValueExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 10");
    list.add("JMP 2");
    list.add("RET R1");
    Machine machine = new Machine();
    machine.execute(list);
  }

  //To test an exception, specify the expected exception after the @Test


  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
