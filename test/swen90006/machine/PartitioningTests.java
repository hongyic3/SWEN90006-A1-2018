package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.




  //Test test opens a file and executes the machine
//  @Test public void aFileOpenTest()
//  {
//    final List<String> lines = readInstructions("examples/array.s");
//    Machine m = new Machine();
//    assertEquals(m.execute(lines), 45);
//  }
  
  //To test an exception, specify the expected exception after the @Test
//  @Test(expected = java.io.IOException.class)
//    public void anExceptionTest()
//    throws Throwable
//  {
//    throw new java.io.IOException();
//  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
//  @Test public void aFailedTest()
//  {
//    //include a message for better feedback
//    final int expected = 2;
//    final int actual = 1 + 2;
//    assertEquals("Some failure message", expected, actual);
//  }


  @Test public void TC1TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("RET R1");
    Machine machine = new Machine();
    int actual = 0;
    assertEquals(machine.execute(list), actual);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC2ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("RET R-2");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC3ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("RET R33");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC4ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("ADD R3 R2 R-2");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC5ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("LDR R1 R2 -66666");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = NoReturnValueException.class)
  public void TC6ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("STR R1 10 R2");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC7ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("LDR R1 R2 66666");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC8ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("SUB R1 R2 R33");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC9ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("CMP R1 R2");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC10ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 2");
    list.add("DIV R33 R2 R1");
    list.add("RET R33");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC11ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("JZ R1 -66666");
    list.add("RET R1");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test public void TC12TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 2");
    list.add("MUL R3 R1 R2");
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 2;
    assertEquals(machine.execute(list), actual);
  }

  @Test public void TC13TestCase()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 2");
    list.add("MUL R3 R1 R2");
    list.add("RET R1");
    list.add("RET R3");
    Machine machine = new Machine();
    int actual = 1;
    assertEquals(machine.execute(list), actual);
  }

  @Test(expected = NoReturnValueException.class)
  public void TC14ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 2");
    list.add("MUL R3 R1 R2");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC15ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("JZ R1 66666");
    list.add("RET R1");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC16ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R-1 -1");
    list.add("RET R-1");
    Machine machine = new Machine();
    machine.execute(list);
  }

  @Test(expected = InvalidInstructionException.class)
  public void TC17ExceptionTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("CMP R1 R2");
    list.add("RET R1");
    Machine machine = new Machine();
    machine.execute(list);
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
